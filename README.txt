CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Maintainers

INTRODUCTION
------------

This module restricts users from deleting the terms that are being referenced
in nodes or by fields.

REQUIREMENTS
------------
This module requires the following modules to be enabled:

 * Taxonomy (in core)

INSTALLATION
------------

1. Download, install and enable the module

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.

MAINTAINERS
-----------

Current maintainers:
* Diome Nike Potot (diomenike@gmail.com) - https://www.drupal.org/user/3449201
* Judy Montesa (montesajudy@gmail.com) - https://drupal.org/user/3443767

CREDITS TO:
-----------
Madelyn Cruz - https://www.drupal.org/user/2523544
